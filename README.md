# Propel Technical Interview - Java Dev

## Values

Remember our values of Owners Mindset, Quality, Forthright conversations, Impact and Client Obsession.

## Tasks - Overview

This is currently a fully functioning spring boot application. You can download and play with the propject using your favourite development environment.

In the interview we will ask you to walk through your solution. We can provide a MacBook with IntellitJ to work through in front of us or feel free to bring your solution in using your own dev environment.

### Part 1: Increase test coverage

  1. Write a set of tests so that the "StudentService.java" class is 100% covered. We are interested in what problems, choices and decisions you make while creating those tests.
